import com.devexperts.dxlab.lincheck.LinChecker;
import com.devexperts.dxlab.lincheck.annotations.Operation;
import com.devexperts.dxlab.lincheck.annotations.Param;
import com.devexperts.dxlab.lincheck.paramgen.IntGen;
import com.devexperts.dxlab.lincheck.strategy.stress.StressCTest;
import org.junit.Test;

@StressCTest
@Param(name = "value", gen = IntGen.class)
public class LinCh {
    private LFSet<Integer> m = new LFSet<Integer>();

    @Operation(params = {"value"})
    public Boolean add(Integer value) {
        return m.add(value);
    }

    @Operation(params = {"value"})
    public Boolean contains(Integer value) {
        return m.contains(value);
    }

    @Test
    public void test() {

        LinChecker.check(LinCh.class);
    }
}
