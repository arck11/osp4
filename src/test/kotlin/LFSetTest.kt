import org.junit.Assert
import org.junit.Test
import java.lang.Thread.sleep
import kotlin.concurrent.thread
import com.devexperts.dxlab.lincheck.LinChecker
import com.devexperts.dxlab.lincheck.LoggingLevel
import com.devexperts.dxlab.lincheck.strategy.stress.StressOptions



class LFSetTest {

    @Test
    fun test_add() {
        val s = LFSet<Int>()
        val value = 1
        var result: Boolean

        Assert.assertNotNull(s)
        Assert.assertEquals(s.isEmpty(), true)

        result = s.add(value)

        Assert.assertEquals(result, true)
        Assert.assertEquals(s.contains(value), true)

        result = s.add(value)

        Assert.assertEquals(result, false)
        Assert.assertEquals(s.contains(value), true)

    }

    @Test
    fun test_remove() {
        val value = 1
        var s = LFSet<Int>()

        var result: Boolean

        Assert.assertNotNull(s)
        Assert.assertEquals(s.isEmpty(), true)

        Assert.assertEquals(s.contains(value), false)

        result = s.add(value)

        Assert.assertEquals(result, true)
        Assert.assertEquals(s.contains(value), true)

        result = s.remove(value)

        Assert.assertEquals(result, true)
        Assert.assertEquals(s.contains(value), false)


        result = s.remove(value)

        Assert.assertEquals(result, false)
        Assert.assertEquals(s.contains(value), false)
    }

    @Test
    fun test_init() {
        var s = LFSet<Int>()

        Assert.assertNotNull(s)
        Assert.assertEquals(s.isEmpty(), true)

        val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9)

        s = LFSet<Int>(numbers)

        Assert.assertNotNull(s)
        Assert.assertEquals(s.isEmpty(), false)

        for (num in numbers) {
            Assert.assertEquals(s.contains(num), true)
        }

    }

    @Test
    fun test_contains() {
        val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        var s = LFSet<Int>()

        for (num in numbers) {
            Assert.assertEquals(s.contains(num), false)
        }

        s = LFSet(numbers)

        for (num in numbers) {
            Assert.assertEquals(s.contains(num), true)
        }

        Assert.assertEquals(s.contains(10), false)
    }

    @Test
    fun test_iterator() {
        val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        var s = LFSet<Int>(numbers)
        var index: Int = 0

        for (num in s) {
            Assert.assertEquals(numbers[index++], num)
        }

        s = LFSet<Int>()
        Assert.assertTrue(s.add(1))

        val it = s.iterator()

        Assert.assertTrue(it.hasNext())
        Assert.assertTrue(s.remove(1))
        Assert.assertEquals(it.next(), 1)

    }

}