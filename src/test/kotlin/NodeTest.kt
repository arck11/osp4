import org.junit.Assert
import org.junit.Test

class NodeTest {
    @Test
    fun test_node() {
        var n = Node<Int>(1)

        Assert.assertEquals(1, n.value)
        Assert.assertNotNull(n.next)
        Assert.assertNull(n.next.reference)
        Assert.assertEquals(n.next.isMarked, false)

        n = Node(null)

        Assert.assertEquals(null, n.value)
        Assert.assertNotNull(n.next)
        Assert.assertNull(n.next.reference)
        Assert.assertEquals(n.next.isMarked, false)
    }
}