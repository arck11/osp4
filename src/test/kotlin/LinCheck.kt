import com.devexperts.dxlab.lincheck.LinChecker
import com.devexperts.dxlab.lincheck.annotations.Param
import com.devexperts.dxlab.lincheck.paramgen.IntGen
import com.devexperts.dxlab.lincheck.strategy.stress.StressCTest
import org.junit.Test
import com.devexperts.dxlab.lincheck.LoggingLevel
import com.devexperts.dxlab.lincheck.annotations.OpGroupConfig
import com.devexperts.dxlab.lincheck.annotations.Operation
import com.devexperts.dxlab.lincheck.strategy.stress.StressOptions
import com.devexperts.dxlab.lincheck.verifier.linearizability.LinearizabilityVerifier
import com.devexperts.dxlab.lincheck.verifier.quiescent.QuiescentConsistent
import org.junit.Assert
import com.devexperts.dxlab.lincheck.verifier.quiescent.QuiescentConsistencyVerifier




@OpGroupConfig(name = "producer", nonParallel = true)
@Param(name = "value", gen = IntGen::class, conf = "1:5")
@StressCTest(
    verifier = LinearizabilityVerifier::class,
    actorsPerThread = 10,
    threads = 2,
    invocationsPerIteration = 10000,
    iterations = 100,
    actorsBefore = 10,
    actorsAfter = 0
)
class LinCheck {
    var m = LFSet<Int>()

    @Operation
    fun add(value: Int) {
        m.add(value)
    }

    @Operation
    fun contains(value: Int) {
        m.contains(value)
    }

    @Operation
    fun remove(value: Int) {
        m.remove(value)
    }

    @Operation
    fun iterate() {
        var x: Int
        for (item in m.iterator()) {
            x = item
            x == item
        }
    }

    @Test
    fun test() {
        val opts = StressOptions()
            .iterations(100)
            .invocationsPerIteration(10)
            .threads(100)
            .actorsPerThread(10)
            .actorsBefore(1)
            .actorsAfter(1)
            .logLevel(LoggingLevel.INFO)
        LinChecker.check(LinCheck::class.java, opts)
    }
}