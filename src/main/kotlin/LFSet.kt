import java.util.concurrent.atomic.AtomicMarkableReference

/**
 * Lock-Free множество.
 * @param <T> Тип ключей
 */
interface ILFSet<T : Comparable<T>> {
    /**
     * Добавить ключ к множеству
     *
     * Алгоритм должен быть как минимум lock-free
     *
     * @param value значение ключа
     * @return false если value уже существует в множестве,
     * true если элемент был добавлен
     */
    fun add(value: T): Boolean


    /**
     * Удалить ключ из множества
     *
     * Алгоритм должен быть как минимум lock-free
     *
     * @param value значение ключа
     * @return false если ключ не был найден, true если ключ успешно удален
     */
    fun remove(value: T): Boolean


    /**
     * Проверка наличия ключа в множестве
     *
     * Алгоритм должен быть как минимум wait-free
     *
     * @param value значение ключа

     * @return true если элемент содержится в множестве, иначе - false
     */
    fun contains(value: T): Boolean

    /**
     * Проверка множества на пустоту
     *
     * Алгоритм должен быть как минимум lock-free
     *
     * @return true если множество пусто, иначе - false
     */
    fun isEmpty(): Boolean

    /**
     * Возвращает lock-free итератор для множества
     *
     * @return новый экземпляр итератор для множества
     */

    fun iterator(): Iterator<T>
}


class Node<T : Comparable<T>>(v: T?) {
    var value: T? = v

    var next: AtomicMarkableReference<Node<T>?> = AtomicMarkableReference(null, false)
}


class LFSetIterator<T : Comparable<T>>(node: Node<T>) : Iterator<T> {

    private var currentNode: Node<T> = node
    private var nextNode: Node<T>? = null

    private fun findNext(node: Node<T>): Node<T>? {
        /* Ищем следующую живую ноду
        * */
        var next = node.next.reference

        while (next != null && next.next.isMarked) {
            next = next.next.reference
        }

        return next
    }

    override fun hasNext(): Boolean {
        /* Ищем следующую ноду
           Фиксируем ссылку на первую не удаленную
        * */
        nextNode = findNext(currentNode)
        return nextNode != null
    }

    override fun next(): T {
        /* Ищем следующую ноду для текущего состояния,
           Если такой нет, возвращаем сохраненную ноду из состояния запроса hasNext
           Так как нам надо вернуть что-то валидное
        * */
        var next = findNext(currentNode)

        if (next == null) {
            // при нормальном использовании итератора nextNode != null,
            // т.к. мы проверили это в hasNext
            next = nextNode
        }
        currentNode = next!!

        return currentNode.value!!
    }

}


class LFSet<T : Comparable<T>>(values: Iterable<T> = emptyList()) : ILFSet<T>, Iterable<T> {

    private var root: Node<T> = Node<T>(null)

    init {
        for (value in values) {
            add(value)
        }
    }

    private fun find(value: T): Pair<Node<T>, Node<T>?> {
        var pred: Node<T>
        var curr: Node<T>?

        retry@ while (true) {
            pred = root
            curr = root.next.reference


            search@ while (true) {

                if (curr == null) {
                    // Если мы дошли до конца списка
                    return Pair(pred, null)
                }
                val next = curr.next

                if (next.isMarked) {
                    if (!pred.next.compareAndSet(curr, next.reference, false, false))
                    // Пробуем перекинуть указатель с pred на next, попутно проверяя что pred еще не удален
                    // Иначе заново пробегаемся с начала списка
                        continue@retry
                    curr = next.reference
                } else {

                    if (curr.value!! >= value)
                    // мы точно значем, что value != null
                    // так как value == null только у root
                        return Pair(pred, curr)

                    pred = curr
                    curr = next.reference
                }
            }
        }

    }

    override fun add(value: T): Boolean {
        var pred: Node<T>
        val newNode: Node<T> = Node(value)
        var curr: Node<T>?
        var p: Pair<Node<T>, Node<T>?>

        retry@ while (true) {
            p = find(value)
            pred = p.first
            curr = p.second

            if (curr?.value == value)
                return false

            newNode.next = AtomicMarkableReference(curr, false)

            if (pred.next.compareAndSet(curr, newNode, false, false))
            // Пробуем вставить новую ноду
            // Попутно проверяя что предок не удален
                return true
        }
    }

    override fun remove(value: T): Boolean {
        var pred: Node<T>
        var next: Node<T>?
        var curr: Node<T>?
        var p: Pair<Node<T>, Node<T>?>

        retry@ while (true) {
            p = find(value)
            pred = p.first
            curr = p.second

            if (curr?.value != value)
                return false

            next = curr.next.reference

            if (!curr.next.compareAndSet(next, next, false, true))
            // Пробуем удалить ноду логически
                continue@retry

            // Если смогли выставить флаг логического удаления
            // Пробуем удалить физически, перекинув ссылку с pred на next
            // Не обязательно ждать успешного удаления!
            pred.next.compareAndSet(curr, next, false, false)
            return true
        }
    }

    override fun contains(value: T): Boolean {
        val (_, curr) = find(value)
        return curr?.value == value
    }

    override fun isEmpty(): Boolean {
        return root.next.reference == null
    }

    override fun iterator(): Iterator<T> {
        return LFSetIterator<T>(root)
    }

}